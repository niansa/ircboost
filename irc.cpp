#include "ircboost/irc.hpp"
#include "logger.hpp"



namespace ChatFuse {
namespace IRC {
void Event::parse(std::string raw) {
    basestr = std::move(raw);

    using namespace Parsing;
    if (basestr.empty()) {
        return;
    }
    // Get string
    std::string_view leftover = basestr;
    // Check event type
    if (basestr[0] == ':') [[likely]] {
        // Remove first character (':')
        leftover = {leftover.data()+1, leftover.size()-1};
        // Get sender
        std::tie(sender, leftover) = splitOnce(leftover, ' ');
        // Get name
        std::tie(name, leftover) = splitOnce(leftover, ' ');
        // Get received
        std::tie(receiver, leftover) = splitOnce(leftover, ' ');
    } else {
        // Get name
        std::tie(name, leftover) = splitOnce(leftover, ' ');
    }
    // Get extra args
    while (!leftover.empty()) {
        // Get data
        if (leftover[0] == ':') [[unlikely]] {
            data = {leftover.data()+1, leftover.size()-1};
            break;
        }
        // Get next arg
        std::string_view arg;
        std::tie(arg, leftover) = splitOnce(leftover, ' ');
        extra_args.push_back(std::string(arg));
    }
}

boost::asio::awaitable<void> Client::onEventLauncher(UEvent event) {
    try {
        auto i = shared_from_this();
        co_await onEvent(std::move(event));
    }  catch (std::exception& e) {
        logger.log(QLog::Loglevel::error, "Uncaught exception in event handler: "+std::string(e.what()));
    }
}

boost::asio::awaitable<std::vector<std::string>> Client::recvLines() {
    std::vector<std::string> fres;
    std::array<char, 16> recv_buf;
    while (fres.empty()) {
        auto len = co_await socket.async_read_some(boost::asio::buffer(recv_buf, recv_buf.size()), boost::asio::use_awaitable);
        for (const auto c : std::span{recv_buf.data(), len}) {
            if (c != '\n') {
                if (c != '\r') {
                    recv_line_buf.push_back(c);
                }
            } else {
                fres.push_back(std::exchange(recv_line_buf, ""));
            }
        }
    }
    co_return fres;
}

boost::asio::awaitable<void> Client::run() {
    unstop();
    std::shared_ptr<Client> i;
    try {
        i = shared_from_this();
    } catch (std::bad_weak_ptr&) {
        logger.log(QLog::Loglevel::error, "My instance needs to be a shared pointer");
        co_return;
    }
    while (isRunning()) {
        try {
            logger.log(QLog::Loglevel::info, "Connecting to the IRC server...");
            // Connect
            co_await socket.async_connect(boost::asio::ip::tcp::endpoint(settings.ip, settings.port), boost::asio::use_awaitable);
            // on_startup
            co_await onStartup();
            // Register
            Command cmd{
                .name = "NICK",
                .args = {settings.name}
            };
            co_await sendCommand(cmd);
            cmd = {
                .name = "USER",
                .args = {settings.name, settings.name, "*"},
                .data = settings.name
            };
            co_await sendCommand(cmd);
            // on_connect
            co_await onConnect();
            // Main loop
            while (isRunning()) {
                for (const auto& line : co_await recvLines()) {
                    auto event = std::make_unique<const Event>(line);
                    if (event->isValid()) {
                        logger.log(QLog::Loglevel::trace, "Received: "+std::string(event->getUnparsed()));
                    } else {
                        logger.log(QLog::Loglevel::warn, "Received invalid event: "+std::string(event->getUnparsed()));
                        continue;
                    }
                    if (event->name == "PING") {
                        co_await handlePing(*event);
                    }
                    boost::asio::co_spawn(io, onEvent(std::move(event)), boost::asio::detached);
                }
            }
        } catch (std::exception& e) {
            logger.log(QLog::Loglevel::error, "Disconnected from the IRC server: "+std::string(e.what()));
        }
    }
}
}
}
