namespace ChatFuse {
namespace IRC {
struct Event;
struct Command;
struct Client;
}
}

#ifndef _IRC_HPP
#define _IRC_HPP
#define BOOST_ASIO_HAS_CO_AWAIT
#include "stoppable.hpp"
#include "logger.hpp"

#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <string_view>
#include <array>
#include <tuple>
#include <memory>
#include <span>
#include <optional>
#include <functional>

#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/use_awaitable.hpp>
#include <boost/asio/awaitable.hpp>
#include <boost/asio/co_spawn.hpp>
#include <boost/asio/detached.hpp>



namespace ChatFuse {
namespace IRC {
namespace Parsing {
inline std::tuple<std::string_view, std::string_view> splitOnce(std::string_view s, char delimiter) {
    // Find the colon
    auto colonPos = s.find(delimiter);
    if (colonPos == s.npos) {
        return {s, ""};
    }
    // Split there
    return {s.substr(0, colonPos), s.substr(colonPos+1, s.size()-colonPos+1)};
}
}

class Event {
    std::string basestr;

public:
    std::string_view sender,
                receiver,
                name,
                data;
    std::vector<std::string_view> extra_args;

    Event(const Event& o) {
        parse(o.basestr);
    }
    Event(Event&& o) :
        basestr(std::move(o.basestr)),
        sender(o.sender),
        receiver(o.receiver),
        name(o.name),
        data(o.data)
    {}
    Event(const std::string& str) {
        parse(str);
    }

    bool isValid() const {
        return !name.empty();
    }

    const std::string& getUnparsed() const {
        return basestr;
    }

    void parse(std::string raw);
};
using UEvent = std::unique_ptr<const Event>;

struct Command {
    std::string name;
    std::vector<std::string> args;
    std::string data;

    std::string dump() const {
        std::ostringstream fres;
        // Add name
        fres << name << ' ';
        // Add args
        for (const auto& arg : args) {
            fres << arg << ' ';
        }
        // Add data
        if (!data.empty()) {
            fres << ':' << data;
        }
        // Return result
        return fres.str();
    }
};

struct Settings {
    boost::asio::ip::address ip;
    unsigned short port = 6667;
    std::string name;
};

class Client : public std::enable_shared_from_this<Client>, AsyncHelpers::Stoppable {
    boost::asio::ip::tcp::socket socket;
    std::string recv_line_buf;

    boost::asio::awaitable<void> onEventLauncher(UEvent event);

protected:
    boost::asio::io_context& io;
    QLog::Logger<Client> logger;

    boost::asio::awaitable<void> run();

public:
    Settings settings;

    virtual boost::asio::awaitable<void> onEvent(UEvent) = 0;
    virtual boost::asio::awaitable<void> onStartup() {co_return;}
    virtual boost::asio::awaitable<void> onConnect() {co_return;}

    Client(boost::asio::io_context& io, Settings settings = {})
        : socket(io), io(io), settings(settings) {
        logger.inst_ptr = this;
    }

    boost::asio::awaitable<void> asyncSleep(time_t milliseconds) {
        co_await boost::asio::steady_timer(io, boost::asio::chrono::milliseconds(milliseconds)).async_wait(boost::asio::use_awaitable);
    }

    boost::asio::awaitable<std::vector<std::string>> recvLines();

    boost::asio::awaitable<void> sendCommand(const Command& cmd) {
        co_await socket.async_write_some(boost::asio::buffer(cmd.dump()+'\n'), boost::asio::use_awaitable);
    }

    virtual boost::asio::awaitable<void> handlePing(const Event& event) {
        Command cmd{
            .name = "PONG",
            .data = std::string(event.data)
        };
        co_await sendCommand(cmd);
    }

    void detach() {
        boost::asio::co_spawn(io, run(), boost::asio::detached);
    }

    QLog::Loglevel& loglevel() {
        return logger.level;
    }
};
}
}
#endif
