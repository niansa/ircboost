cmake_minimum_required(VERSION 3.5)

project(ircboost LANGUAGES C CXX)
set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_library(${PROJECT_NAME} STATIC
    irc.cpp
)

include(CPM.cmake)
CPMAddPackage("gl:chatfuse/libs/async-helpers@2.0.1")
CPMAddPackage("gl:chatfuse/libs/quicklog@1.1")

target_include_directories(${PROJECT_NAME} PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include" "${async-helpers_SOURCE_DIR}" "${quicklog_SOURCE_DIR}")

legacy_coro_fix(${PROJECT_NAME})
